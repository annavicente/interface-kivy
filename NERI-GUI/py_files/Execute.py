# -*- coding: utf-8 -*-

from kivy.uix.floatlayout import FloatLayout
from py_files.Popup_help import *
from py_files.Hint import *
from kivy.lang import Builder
Builder.load_file('kv_files/execute.kv')

class Execute(FloatLayout):

    ''' instance "Popup_help" passing the file name and call the open popoup function'''
    def open_help(self, filename):
        p = Popup_help(filename)
        p.open()

    def __init__(self,  **kwargs):
        super(Execute, self).__init__(**kwargs)
        self.help = TooltipHint(self.ids['help'].text)
