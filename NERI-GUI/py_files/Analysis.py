# -*- coding: utf-8 -*-

from kivy.uix.floatlayout import FloatLayout
from kivy.properties import ObjectProperty
from kivy.uix.popup import Popup
from py_files.Popup_help import *
from py_files.Hint import *
from kivy.lang import Builder
Builder.load_file('kv_files/analysis.kv')


class LoadDialog(FloatLayout):
    '''
        Class responsible for add the window search files.
    '''

    load = ObjectProperty(None)
    cancel = ObjectProperty(None)
    id_input = ''

class Analysis(FloatLayout):

    '''
        Class responsible for manage the Analysis' screen functions.
        Class responsible for the New Experiment screen's functions
        The methods dismiss_popup, show_load and load are responsible for
        to manage the window search files.
        The method open_help open the window when the question icon is clicked.
    '''

    def __init__(self,  **kwargs):
        super(Analysis, self).__init__(**kwargs)
        self.file = TooltipHint(self.ids['hint'].text)


    def open_help(self, nomeArq):
        p = Popup_help(nomeArq)
        p.open()


    def dismiss_popup(self):
        self._popup.dismiss()


    def show_load(self, id_input, rootpath):
        content = LoadDialog(load=self.load, cancel=self.dismiss_popup)
        content.id_input = id_input
        content.ids['filechooser'].rootpath = rootpath
        self._popup = Popup(title="Load file", content=content,
                            size_hint=(0.9, 0.9))
        self._popup.open()


    def load(self, path, filename):
        self.ids[self._popup.content.id_input].text = "%s"%(filename[0])
        self.dismiss_popup()
