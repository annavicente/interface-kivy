# -*- coding: utf-8 -*-

from kivy.lang import Builder
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.popup import Popup
from kivy.uix.screenmanager import Screen, ScreenManager
from py_files.About import *
from py_files.Help import *
from py_files.Contact_us import *

Builder.load_file('kv_files/menu_help.kv')

class MenuHelp(Screen):

    '''open the help screen'''
    def open_help(self):
        content = Help()
        self._popup = Popup(title="Help", content=content,
                            size_hint=(0.9, 0.9))
        self._popup.open()

        '''add the close function to popup's close button'''
        #content.ids['close'].bind(on_release= self._popup.dismiss)

    '''open the about screen'''
    def open_about(self):
        content = About()
        self._popup = Popup(title="About", content=content,
                            size_hint=(0.9, 0.9))
        self._popup.open()

        '''add the close function to popup's close button'''
        #content.ids['close'].bind(on_release= self._popup.dismiss)

    '''open the contact us screen'''
    def open_contact_us(self):
        content = Contact_us()
        self._popup = Popup(title="Contact us", content=content,
                            size_hint=(0.9, 0.9))
        self._popup.open()


    def __init__(self, **kwargs):
        super(MenuHelp, self).__init__(**kwargs)
