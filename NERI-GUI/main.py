from kivy.app import App
from kivy.uix.tabbedpanel import TabbedPanelHeader, TabbedPanelItem
from kivy.lang import Builder
from kivy.core.window import Window

#Window.fullscreen = False

from py_files.NewExperiment import *
from py_files.Input import *
from py_files.Analysis import *
from py_files.Execute import *
from py_files.MenuHelp import *



Builder.load_string(
'''
<RootWidget>
    TabbedPanel:
        id: main
        do_default_tab: True
'''
)

class RootWidget(FloatLayout):

    def __init__(self,  **kwargs):
        super(RootWidget, self).__init__(**kwargs)

        self.ids['main'].default_tab_text = 'New'

        execute = TabbedPanelItem(text='Execute')
        analysis = TabbedPanelItem(text='Analysis')
        help = TabbedPanelItem(text='Help')

        self.ids['main'].add_widget(execute)
        self.ids['main'].add_widget(analysis)
        self.ids['main'].add_widget(help)

        self.ids['main'].default_tab_content = Input()
        analysis.content = Analysis()
        execute.content = Execute()
        help.content = MenuHelp()



class MainApp(App):
    def build(self):
        return RootWidget()

if __name__ == '__main__':
    MainApp().run()
